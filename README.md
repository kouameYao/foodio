# Foodio

Manger saint pour garder notre corps saint

### Lancement du projet

D'abord, exécutez selon vos préférences

```bash
npm i && npm run dev
# ou
yarn && yarn dev
```

Ouvrez [http://localhost:3000](http://localhost:3000) dans votre navigateur préférenré.

Foodio en production [eat-foodio.vercel.app](https://eat-foodio.vercel.app/).

### Pour J. P, ou si toi aussi tu utilise react

```json
    "react-helmet": "^6.1.0",
    "react-datepicker": "^4.5.0",
    "react-tabs": "^4.2.0",
    "react-rating-stars-component": "^2.2.0"
```

- Télécharge les assets [sur mon drive](https://drive.google.com/drive/folders/1l-4a4uWqsQze0JsBQUMdUk3ZwwbBJCGJ?usp=sharing)

- Tu peux utiliser [vercel.com](https://vercel.com/dashboard) pour le déploiement

- Pour les icons : [iconify](https://icon-sets.iconify.design/)
