import { Button } from "@/components/ui/button";
import Image from "next/image";

export default function AboutUsPage() {
  return (
    <div className="container">
      <div className="grid grid-cols-2 sm:grid-rows-1 pt-5 py-10 gap-20">
        <div className="w-full">
          <div className="h-[545px] w-[540px] center-child bg-gray-200 rounded-full">
            <div className="bg-gray-300 h-[441px] w-[446px] center-child rounded-full">
              <Image
                src="/images/img_about_heading.png"
                alt=""
                width={346}
                height={346}
              />
            </div>
          </div>
        </div>
        <div className="w-full flex flex-col justify-center gap-5">
          <div className="flex flex-col gap-2">
            <h1 className="h1-bold">About Our</h1>
            <h1 className="h1-red">Restaurant</h1>
          </div>
          <p className="p-medium">
            This dish is full of flavor and nutrition! <br /> Quinoa is a
            complete protein, providing all <br /> the essential amino acids
            your body <br /> needs, and is also a good source of fiber
          </p>
          <div className="flex flex-row sm:flex-col gap-5 mt-10">
            <Button className="btn-primary">Order now</Button>
          </div>
        </div>
      </div>

      {/*  */}

      <div className="grid grid-cols-2 sm:grid-rows-1 py-10 gap-20">
        <div className="w-full flex flex-col justify-center gap-5">
          <p className="p-medium">
            Sed ut perspiciatis unde omnis iste natus <br /> error sit
            voluptatem accusantium <br /> doloremque laudantium, totam rem{" "}
            <br /> aperiam, eaque ipsa quae ab illo inventore <br /> veritatis
            et quasi architecto beatae vitae <br />
            dicta sunt explicabo. Nemo enim ipsam <br /> voluptatem quia
            voluptas sit aspernatur aut <br /> odit aut fugit.
          </p>
        </div>
        <div className="w-full">
          <div className="h-[545px] w-[540px] center-child bg-gray-200 rounded-full">
            <div className="bg-gray-300 h-[441px] w-[446px] center-child rounded-full">
              <Image
                src="/images/img_about_2.png"
                alt=""
                width={346}
                height={346}
              />
            </div>
          </div>
        </div>
      </div>

      {/* Owner section */}

      <div className="grid grid-cols-2 sm:grid-rows-1 my-32 gap-20">
        <div className="w-full">
          <div className="h-[441px] w-[446px] center-child">
            <Image
              src="/images/img_owner.png"
              alt=""
              width={467}
              height={639}
            />
          </div>
        </div>
        <div className="w-full flex flex-col justify-center gap-5">
          <div className="flex gap-2">
            <h1 className="h1-red">Owner</h1>
            <h1 className="h1-bold">& Executive</h1>
          </div>
          <h1 className="h1-bold">Chef</h1>
          <h5 className="font-medium text-gray-800 opacity-80 text-3xl my-5">
            Ismail Marzuki
          </h5>
          <div className="bg-[url('/images/img_pattern_testimonial.svg')] bg-no-repeat flex flex-col pt-10 gap-5">
            <p className="font-medium text-gray-800 text-2xl">
              Lorem ipsum dolor sit amet,
            </p>
            <p className="font-medium text-gray-800 text-2xl">
              consectetur adipiscing elit, sed.
            </p>
            <p className="font-medium text-gray-800 text-2xl">
              do eiusmod tempor incididunt ut
            </p>
            <p className="font-medium text-gray-800 text-2xl">
              labore et dolore magna aliqua.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
