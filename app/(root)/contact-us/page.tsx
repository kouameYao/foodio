import { MapBox } from "@/components/map";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Textarea } from "@/components/ui/textarea";

export default function ContactUsPage() {
  return (
    <div className="container mt-5">
      <h1 className="h1-bold flex center-child ">Contact us</h1>
      <div className="flex flex-col items-center text-xl mt-5">
        <p>
          We love hearing from our customers. Feel free to share your experience
          or ask
        </p>
        <p>any questions you may have.</p>
      </div>
      <div className="flex flex-row mt-16 gap-10 sm:flex-col">
        <div className="h-[550px] w-[48%] rounded-2xl">
          <MapBox />
        </div>
        <div className="w-[45%] flex flex-col gap-8">
          <Input className="p-6" placeholder="First name" />
          <Input className="p-6" placeholder="Last name" />
          <Input className="p-6" type="email" placeholder="Email address" />
          <Input className="p-6" placeholder="Subject" />
          <Textarea rows={5} placeholder="Message" />
          <Button className="btn-primary">Submit</Button>
        </div>
      </div>
    </div>
  );
}
