import { Header } from "@/components/share/header";
import { Footer } from "@/components/share/footer";

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <div className="flex flex-col min-h-screen">
      <div className="flex-1 container">
        <Header />
      </div>
      <main>{children}</main>
      <Footer />
    </div>
  );
}
