import { MenuCategories } from "@/components/share/menu-categories";
import { MenuPagination } from "@/components/share/pagination";
import { PopularMenuItem } from "@/components/share/popular-menu-item";
import { menuItems } from "@/constants/menus";

export default function MenuPage() {
  return (
    <div className="container text-center">
      <p className="font-bold text-6xl my-20 text-gray-900">Our Popular Menu</p>
      <div>
        <MenuCategories />
      </div>
      <div className="grid grid-cols-3 md:grid-cols-1 gap-y-10 md:gap-x-10 sm:grid-cols-1 sm:gap-5">
        {menuItems.map((item, index) => (
          <PopularMenuItem key={index} {...item} />
        ))}
      </div>
      <MenuPagination />
    </div>
  );
}
