import { HeaderSection } from "@/components/share/header-section";
import { MenuCategories } from "@/components/share/menu-categories";
import { MenuPagination } from "@/components/share/pagination";
import { PopularChefCard } from "@/components/share/popular-chef-card";
import { PopularMenuItem } from "@/components/share/popular-menu-item";
import { Button } from "@/components/ui/button";
import { chefItems } from "@/constants/chefs";
import { menuItems } from "@/constants/menus";
import Image from "next/image";

export default function HomePage() {
  return (
    <>
      <HeaderSection />
      <div className="my-28 py-10 bg-green-50">
        <div className="container h-full flex flex-row justify-between items-center gap-10 sm:flex-col">
          <Image
            width={617}
            height={610}
            src="/images/img_kindpng_3443995.png"
            alt="heading section image"
          />
          <div className="flex flex-col gap-5">
            <h1 className="font-bold text-5xl">
              Our Most
              <span className="text-red-A700_01"> Dish.</span>
            </h1>
            <h1 className="font-bold text-5xl"> Popular </h1>
            <div className="flex flex-col gap-3 mt-5">
              <p className="font-medium text-xl">
                This dish is full of flavor and nutrition! Quinoa is
              </p>
              <p className="font-medium text-xl">
                a complete protein, providing all the essential
              </p>
              <p className="font-medium text-xl">
                amino acids your body needs, and is also
              </p>
              <p className="font-medium text-xl">a good source of fiber.</p>
            </div>
            <div className="mt-10">
              <Button className="btn-primary">Order now</Button>
            </div>
          </div>
        </div>
      </div>
      <div className="container text-center">
        <p className="font-bold text-5xl mb-20">Our Popular Menu</p>
        <MenuCategories />
        <div className="grid grid-cols-3 md:grid-cols-1 gap-y-10 md:gap-x-10 sm:grid-cols-1 sm:gap-5">
          {menuItems.map((item, index) => (
            <PopularMenuItem key={index} {...item} />
          ))}
        </div>
        <MenuPagination />
      </div>
      <div className="container text-center mt-28">
        <p className="font-bold text-4xl mb-20">Our Popular Chefs</p>
        <div className="grid grid-cols-3 md:grid-cols-1 w-full gap-y-10 sm:grid-cols-1 sm:gap-5">
          {chefItems.map((item, index) => (
            <PopularChefCard key={index} {...item} />
          ))}
        </div>
        <Button className="btn-primary mt-20">View all</Button>
      </div>
      {/* <div className="container flex flex-col justify-center text-center mt-40">
        <p className="font-bold text-5xl mb-20">What Our Customers Say</p>
        <TestimonialCarousel />
      </div> */}
      <div className="container mt-28 mb-20">
        <div className="flex flex-col items-center bg-red-100 px-40 py-28 rounded-2xl">
          <p className="font-bold text-5xl mb-10">Hungry? We are open now..</p>

          <div className="flex flex-row sm:flex-col gap-5">
            <Button className="btn-primary">Order now</Button>
            <Button className="btn-tertiary">Reservation</Button>
          </div>
        </div>
      </div>
    </>
  );
}
