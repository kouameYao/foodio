import type { Metadata } from "next";
import { Poppins } from "next/font/google";
import "./globals.css";

const poppins = Poppins({ subsets: ["latin"], weight: ["400", "600"] });

export const metadata: Metadata = {
  title: "Foodio | Nourrissez vous en penssant à votre santé",
  description: "Application pour la vente de nourriture en ligne",
  applicationName: "Foodio",
  authors: [{ name: "kouameYao", url: "" }],
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={poppins.className}>
        <div className="h-full bg-gray-50">{children}</div>
      </body>
    </html>
  );
}
