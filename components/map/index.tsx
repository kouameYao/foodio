"use client";

import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";

import "mapbox-gl/dist/mapbox-gl.css";
import { MAPBOX_API } from "@/lib/config-global";

const Map = ReactMapboxGl({
  accessToken: MAPBOX_API!,
});

export const MapBox = () => {
  const popupInfo = {
    latlng: [5.353446, -3.941749],
    address: "",
    phoneNumber: "string",
  };

  return (
    <Map
      style="mapbox://styles/mapbox/streets-v9"
      containerStyle={{
        height: "100%",
        width: "100%",
        borderRadius: "10px",
      }}
      center={[popupInfo?.latlng[1], popupInfo?.latlng[0]]}
    >
      <Layer type="symbol" id="marker" layout={{ "icon-image": "marker-15" }}>
        <Feature coordinates={[popupInfo?.latlng[1], popupInfo?.latlng[0]]} />
      </Layer>
    </Map>
  );
};
