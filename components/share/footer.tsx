import Link from "next/link";
import Image from "next/image";
import React from "react";

import { socialLinks } from "@/constants/socials-media";
import {
  footerPageItems,
  footerInformationItems,
  footerGetInTouchItems,
} from "@/constants/nav-config";

export const Footer = () => {
  return (
    <div className="w-full bg-gray-900_01 text-gray-300_01 px-44 md:px-10 py-20 mt-20">
      <div className="grid grid-cols-4 gap-5 md:grid-cols-2 sm:grid-cols-1">
        <div className="flex flex-col gap-5">
          <Link href="/">
            <Image
              src="/images/img_footer_logo.svg"
              alt="Foodio logo"
              width={120}
              height={100}
              priority
            />
          </Link>
          <div className="p-bold text-lg my-5 leading-10">
            <p> Viverra gravida morbi egestas </p>
            <p> facilisis tortor netus non duis </p>
            <p> tempor. </p>
          </div>

          <div className="flex flex-row gap-5">
            {socialLinks.map((link, index) => (
              <Link
                key={index}
                href={link.href}
                className="w-14 h-14 flex justify-center items-center bg-gray-50 rounded-full cursor-pointer"
              >
                <Image src={link.src} alt={link.alt} width={30} height={30} />
              </Link>
            ))}
          </div>
        </div>
        <div className="flex flex-col px-5 ml-10">
          <h3 className="font-semibold text-2xl text-red-400">Page</h3>
          <div className="flex flex-col mt-10 gap-6">
            {footerPageItems.map((item, index) => (
              <Link key={index} href={item.link} className="text-lg">
                {item.label}
              </Link>
            ))}
          </div>
        </div>
        <div className="flex flex-col">
          <h3 className="font-semibold text-2xl text-red-400">Information</h3>
          <div className="flex flex-col mt-10 gap-6">
            {footerInformationItems.map((item, index) => (
              <Link key={index} href={item.link} className="text-lg">
                {item.label}
              </Link>
            ))}
          </div>
        </div>
        <div className="flex flex-col">
          <h3 className="font-semibold text-2xl text-red-400">Get in touch</h3>
          <div className="flex flex-col mt-10 gap-6">
            {footerGetInTouchItems.map((item, index) => (
              <Link key={index} href={item.link} className="text-lg">
                {item.label}
              </Link>
            ))}
          </div>
        </div>
      </div>
      <p className="text-center text-lg pt-14">Copyright © mars 2025</p>
    </div>
  );
};
