import React from "react";
import Image from "next/image";

import { Button } from "../ui/button";

export const HeaderSection = () => {
  return (
    <div className="container flex flex-row justify-between py-10 sm:flex-col">
      <div className="flex flex-col gap-5">
        <h1 className="font-bold text-7xl">Best </h1>
        <h1 className="font-bold text-7xl"> Restaurant </h1>
        <h1 className="font-bold text-7xl">
          In <span className="text-red-A700_01"> Town.</span>{" "}
        </h1>
        <div className="flex flex-col gap-2">
          <p className="font-medium text-xl">
            We provide best food in town, we provide home
          </p>
          <p className="font-medium text-xl">delivery and dine in services.</p>
        </div>
        <div className="flex flex-row gap-5 mt-10">
          <Button className="btn-primary">Order now</Button>
          <Button className="btn-secondary">Reservation</Button>
        </div>
      </div>
      <Image
        width={590}
        height={510}
        src="/images/heading-image.png"
        alt="heading section image"
      />
    </div>
  );
};
