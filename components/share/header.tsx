import Image from "next/image";
import React from "react";
import { Button } from "../ui/button";
import Link from "next/link";
import { NavList } from "./nav-list";

export const Header = () => {
  return (
    <div className="flex flex-rows items-center justify-between py-12">
      <Link href="/">
        <Image
          src="/images/img_logo.svg"
          alt="Foodio logo"
          width={120}
          height={100}
          priority
        />
      </Link>
      <NavList />
      <div className="flex items-center gap-5">
        <div className="w-12 h-12 flex justify-center items-center bg-white-A700 rounded-full cursor-pointer">
          <Image
            src="/images/img_basket.svg"
            alt="Order icon"
            width={25}
            height={25}
          />
        </div>
        <Button
          className="p-6 rounded-2xl text-lg text-white-A700 bg-red-400"
          aria-label="Login button"
        >
          Log in
        </Button>
      </div>
    </div>
  );
};
