"use client";

import React, { useState } from "react";
import { ToggleGroup, ToggleGroupItem } from "@/components/ui/toggle-group";
import { CategoryValue } from "@/types/category";
import { categories } from "@/constants/categories";

export function MenuCategories() {
  const [category, setCategory] = useState(categories[0].value);

  const handleCategoryChange = (value: CategoryValue) => {
    setCategory(value);
  };

  return (
    <ToggleGroup
      type="single"
      className="grid grid-cols-5 sm:grid-rows-1 mb-10"
    >
      {categories.map((cat) => (
        <ToggleGroupItem
          key={cat.value}
          value={cat.value}
          aria-label={`Toggle ${cat.label}`}
          onClick={() => handleCategoryChange(cat.value)}
          className={
            category === cat.value ? "active-category" : "not-active-category"
          }
        >
          <p className="text-lg">{cat.label}</p>
        </ToggleGroupItem>
      ))}
    </ToggleGroup>
  );
}
