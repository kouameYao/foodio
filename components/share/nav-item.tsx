"use client";

import { NavigationItem } from "@/types/nav-item";
import Link from "next/link";
import { usePathname } from "next/navigation";
import React from "react";

const NavItem = ({ link, label }: NavigationItem) => {
  const pathname = usePathname();
  const isActive = pathname === link;

  return (
    <li
      className={`${
        isActive && "border-b-2 border-red-A700 pb-1 text-red-A700"
      } hover:border-b-2 hover:border-red-A700 pb-1 `}
    >
      <Link href={link}>{label}</Link>
    </li>
  );
};

export default NavItem;
