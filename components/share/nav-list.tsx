import { navigationItems } from "@/constants/nav-config";
import React from "react";
import NavItem from "./nav-item";
import { cn } from "@/lib/utils";

export const NavList = ({ className }: { className?: string }) => {
  return (
    <nav>
      <ul className={cn("flex gap-10", className)}>
        {navigationItems.map((item, index) => {
          return <NavItem key={index} {...item} />;
        })}
      </ul>
    </nav>
  );
};
