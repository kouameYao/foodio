"use client";

import React, { useState } from "react";
import {
  Pagination,
  PaginationContent,
  PaginationEllipsis,
  PaginationItem,
  PaginationLink,
} from "@/components/ui/pagination";
import Image from "next/image";

export function MenuPagination() {
  const [page, setPage] = useState(1);

  const handlePageChange = (pageNumber: number) => {
    setPage(pageNumber);
  };

  const renderPaginationItems = () => {
    const items = [];
    for (let i = 1; i <= 3; i++) {
      items.push(
        <PaginationItem key={i} onClick={() => handlePageChange(i)}>
          <PaginationLink
            isActive={page === i}
            onClick={() => handlePageChange(i)}
            className={
              page === i ? "active-pagination" : "not-active-pagination"
            }
          >
            {i}
          </PaginationLink>
        </PaginationItem>
      );
    }
    return items;
  };

  return (
    <Pagination className="my-14">
      <PaginationContent>
        <PaginationItem>
          <Image
            src="/images/img_arrow_left.svg"
            alt="icon left"
            width={20}
            height={20}
          />
        </PaginationItem>
        {renderPaginationItems()}
        <PaginationItem>
          <PaginationEllipsis />
        </PaginationItem>
        <PaginationItem>
          <Image
            src="/images/img_arrow_right.svg"
            alt="icon left"
            width={20}
            height={20}
          />
        </PaginationItem>
      </PaginationContent>
    </Pagination>
  );
}
