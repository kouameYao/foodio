import React from "react";
import Image from "next/image";

import { ChefCardProps } from "@/types";

export const PopularChefCard = ({
  fullname,
  role,
  bgcolor,
  imageSrc,
}: ChefCardProps) => {
  return (
    <div className="flex flex-col items-center justify-center gap-5">
      <div
        style={{ backgroundColor: bgcolor }}
        className="flex flex-col items-center justify-center gap-7 rounded-[2rem]"
      >
        <Image width="350" height="300" src={imageSrc} alt={fullname} />
      </div>
      <p className="font-semibold text-xl text-gray-950">{fullname}</p>
      <p className="font-medium text-md text-gray-600">{role}</p>
    </div>
  );
};
