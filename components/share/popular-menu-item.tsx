import React from "react";
import Image from "next/image";

import { MenuItem } from "@/types";

import { Button } from "../ui/button";

export const PopularMenuItem = ({
  title,
  description,
  imageSrc,
  price,
}: MenuItem) => {
  return (
    <div className="flex flex-col items-center gap-7 bg-white-A700 w-[347px] p-[30px] rounded-[2rem]">
      <Image width={270} height={270} src={imageSrc} alt={title} />
      <p className="font-semibold text-3xl">{title}</p>
      <p className="font-medium text-sm">{description}</p>
      {/* <Rating /> */}
      <div className="flex flex-row items-center gap-5">
        <p className="font-semibold text-3xl">{price}</p>
        <Button className="btn-primary-sm">Order now</Button>
      </div>
    </div>
  );
};
