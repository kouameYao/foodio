import { Category } from "@/types/category";

export const categories: Category[] = [
  { value: "ALL_CATEGORY", label: "All categories" },
  { value: "DINNER", label: "Dinner" },
  { value: "LUNCH", label: "Lunch" },
  { value: "DESSERT", label: "Dessert" },
  { value: "DRINK", label: "Drink" },
];
