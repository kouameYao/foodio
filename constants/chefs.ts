import { ChefCardProps } from "@/types";

export const chefItems: ChefCardProps[] = [
  {
    imageSrc: "/images/head-chef.png",
    fullname: "Betran comar",
    role: "Head Chef",
    bgcolor: "#1B161D",
  },
  {
    imageSrc: "/images/chef-1.png",
    fullname: "Ferry sauwi",
    role: "Chef",
    bgcolor: "#FF8900",
  },
  {
    imageSrc: "/images/chef-2.png",
    fullname: "Iswang Dracho",
    role: "Chef",
    bgcolor: "#9D6542",
  },
];
