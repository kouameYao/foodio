export const menuItems = [
  {
    imageSrc: "/images/spaghetti.png",
    title: "Spaghetti",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Egestas consequat mi eget auctor aliquam, diam.",
    price: "$12.05",
  },
  {
    imageSrc: "/images/img_pngitem_1447549.png",
    title: "Gnocchi",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Egestas consequat mi eget auctor aliquam, diam.",
    price: "$12.05",
  },
  {
    imageSrc: "/images/img_pngegg.png",
    title: "Spaghetti",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Egestas consequat mi eget auctor aliquam, diam.",
    price: "$12.05",
  },
  {
    imageSrc: "/images/img_pngwing.png",
    title: "Penne Alla Vodak",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Egestas consequat mi eget auctor aliquam, diam.",
    price: "$12.05",
  },
  {
    imageSrc: "/images/img_pngitem_5290903.png",
    title: "Risoto",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Egestas consequat mi eget auctor aliquam, diam.",
    price: "$12.05",
  },
  {
    imageSrc: "/images/img_pngwing_270x270.png",
    title: "Splitza Signature",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Egestas consequat mi eget auctor aliquam, diam.",
    price: "$12.05",
  },
];
