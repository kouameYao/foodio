import { NavigationItem } from "@/types/nav-item";

export const navigationItems: NavigationItem[] = [
  { link: "/", label: "Home" },
  { link: "/menu", label: "Menu" },
  { link: "/about-us", label: "About us" },
  { link: "/order-online", label: "Order online" },
  { link: "/reservation", label: "Reservation" },
  { link: "/contact-us", label: "Contact us" },
];

export const footerPageItems: NavigationItem[] = [
  { link: "/", label: "Home" },
  { link: "/menu", label: "Menu" },
  { link: "/order-online", label: "Order online" },
  { link: "/about-us", label: "Catering" },
  { link: "/reservation", label: "Reservation" },
];

export const footerInformationItems: NavigationItem[] = [
  { link: "/about-us", label: "About us" },
  { link: "/#testimonial", label: "Testimonial" },
  { link: "/contact-us", label: "Constact us" },
];

export const footerGetInTouchItems: NavigationItem[] = [
  { link: "", label: "2972 Westheimer Rd. Santa Ana, Illinois 85486" },
  { link: "mailto:abc@example.com", label: "abc@example.com" },
  { link: "tel:+123 4567 8901", label: "+123 4567 8901" },
];
