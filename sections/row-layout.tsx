import { cn } from "@/lib/utils";

type RowLayoutProps = { children: React.ReactNode; className?: string };

export function RowLayout({ children, className }: RowLayoutProps) {
  return (
    <div className="container">
      <div className={cn("grid grid-cols-2 sm:grid-rows-1 py-10", className)}>
        {children}
      </div>
    </div>
  );
}
