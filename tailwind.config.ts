import type { Config } from "tailwindcss";

const config = {
  darkMode: ["class"],
  content: [
    "./pages/**/*.{ts,tsx}",
    "./components/**/*.{ts,tsx}",
    "./app/**/*.{ts,tsx}",
    "./src/**/*.{ts,tsx}",
  ],
  prefix: "",
  theme: {
    screens: { md: { max: "1050px" }, sm: { max: "550px" } },
    extend: {
      colors: {
        gray: {
          50: "#f9f9f9",
          200: "#ececec",
          300: "#e3e2e0",
          400: "#c4c4c4",
          500: "#a0978c",
          800: "#5c4429",
          900: "#311f09",
          "900_01": "#1b1919",
          "300_01": "#e3e1df",
          "900_cc": "#311f09cc",
          "800_01": "#5c4529",
          "500_01": "#a08d76",
          "900_02": "#301e08",
          "900_04": "#1b161d",
          "800_02": "#59442b",
          "50_01": "#fafaf9",
          "400_01": "#d0c7c7",
        },
        red: {
          100: "#fdd9d9",
          400: "#f54748",
          "400_19": "#f5474819",
          A700: "#ff0000",
          A700_01: "#ea1010",
        },
        blue_gray: { "100_02": "#ddd4d4", "100_01": "#d0ccc7" },
        green: { 50: "#e7f7ed" },
        white: { A700: "#ffffff", A700_7f: "#ffffff7f" },
        lime: { 900: "#9d6542" },
        black: { 900: "#000000" },
        light_blue: { 800: "#0874c3", A700: "#0074ff" },
        blue: { 400: "#53a5e0" },
        indigo: { 300: "#739fe0", 900: "#123968" },
      },
      boxShadow: {
        xs: "0px 4px  11px 0px #0a0a0a0f",
        sm: "0px 4px  11px 0px #ff38380f",
      },
      fontFamily: {
        poppins: "Poppins",
        opensans: "Open Sans",
      },
      backgroundImage: {
        gradient: "linear-gradient(180deg, #f447482b, #f5474800)",
        gradient1: "linear-gradient(180deg, #ff8900, #ff8900)",
        "bg-pattern": "url('/images/img_pattern_testimonial.svg')",
      },
    },
  },
  plugins: [require("tailwindcss-animate")],
} satisfies Config;

export default config;
