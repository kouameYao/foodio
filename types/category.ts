export enum ECategory {
  ALL_CATEGORY = "ALL_CATEGORY",
  DINNER = "DINNER",
  LUNCH = "LUNCH",
  DESSERT = "DESSERT",
  DRINK = "DRINK",
}

export type CategoryValue = keyof typeof ECategory;

export type Category = {
  value: CategoryValue;
  label: string;
};
