export type MenuItem = {
  imageSrc: string;
  title: string;
  description: string;
  price: string;
};

export type ChefCardProps = {
  imageSrc: string;
  fullname: string;
  role: string;
  bgcolor: string;
};
