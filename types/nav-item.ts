export type NavigationItem = {
  link: string;
  label: string;
};

export type FooterItems = {
  navs: NavigationItem[];
  title: string;
};
